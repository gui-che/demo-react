// var converter = new showdown.Converter();
// 通过props，就能够从中读取到从 CommentList传递过来的数据 
// 任何内嵌的元素作为 this.props.children
var Comment = React.createClass({
    // var rawMarkup = converter.makeHtml( this.props.children.toString() );
    rawMarkup: function() {
        var rawMarkup = marked( this.props.children.toString(), { sanitize: true } );
        // console.log( rawMarkup );
        return { __html: rawMarkup };
    },

    render: function() {
        return (
            <div className="comment">
                <h2 className="commentAuthor">
                    { this.props.author }
                </h2>
                <span dangerouslySetInnerHTML={ this.rawMarkup() } />
            </div>
        );
    }
});
var data = [{
        author: "Pete Hunt",
        text: "This is oen comment"
    }, {
        author: "Jordan Walke",
        text: "This is *another* comment"
    }];

// 通过Javascript对象传递一些方法到React.createClassf() 来创建一个新的React组件。其中最重要的方法就是render，该方法返回一颗React组件树，这棵树最终将会渲染成HTML。
// 这个 <div> 标签不是真实的DOM节点；他们是React div组件的实例。React是安全的。我们不生成HTML字符串，因此默认阻止了XSS攻击。

var CommentList = React.createClass({
    render: function () {
        // console.log( this.props.data );
        var commentNodes = this.props.data.map( function ( comment ) {
            // console.log( comment.author );
            // console.log( comment.text );
            return (
                <Comment author={ comment.author }>
                    { comment.text }
                </Comment>
            );
        });
        return (
            <div className="commentList">
                { commentNodes }
            </div>
        );
    }    
});


var CommentForm = React.createClass({
    handleSubmit: function ( e ) {
        e.preventDefault();
        
        var author = this.refs.author.getDOMNode().value.trim();
        var text = this.refs.text.getDOMNode().value.trim();

        if ( !text || !author ) {
            return ;
        }

        // TODO: send request to the server
        this.props.onCommentSubmit({
            author: author,
            text: text
        });
        this.refs.author.getDOMNode().value = '';
        this.refs.text.getDOMNode().value = '';
        return ;
           
    },

    render: function () {
        // React使用驼峰命名规范的方式给组件绑定事件处理器。
        // 利用Ref属性给予组件命名，this.refs引用组件。我们可以在组件上调用getDOMDode()获取浏览器本地的DOM元素。
        return (
            <form className="commentForm" onSubmit={ this.handleSubmit } >
                <input type="text" placeholder="Your name" ref="author" />
                <input type="text" placeholder="Say something" ref="text" />
                <input type="submit" value="Post" />
            </form>
        );
    }
});

// props是不可变的：它们从父节点传递过来，被父节点"拥有"。为了实现交互，我们给组件引进了可变的state。this.state是组件私有的，可以通过调用this.setState()来改变它。当状态更新后，组件重新渲染自己。
var CommentBox = React.createClass({
    loadCommentsFromServer: function () {
        $.ajax({
            url: this.props.url,
            dataType: 'json',

            success: function ( data ) {
                console.log( data );
                this.setState({
                    data: data 
                });
            }.bind( this ),

            error: function ( xhr, status, err ) {
                console.error( this.props.url, status, err.toString() );
                // console.log( status );
                // console.log( err );
                // console.log( xhr );
            }.bind( this )
        });
    },

    // getInitialState()在组件的生命周期里仅执行一次，设置组件的初始化状态。
    getInitialState: function () {
        return {
            data: []
        };
    },

    handleCommentSubmit: function ( comment ) {
        // TODO: submit to the server and refresh the list
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'POST',
            data: comment,

            success: function ( returnData ) {
                this.setState({
                    data: returnData
                });
            }.bind( this ),

            error: function ( xhr, status , err ) {
                // console.error( this.props.url, status, err.toString() );
                console.log( this.props.url, status, err.toString() );
            }.bind( this )
        });
    },

    componentDidMount: function () {
        // 轮询
        this.loadCommentsFromServer();
        setInterval( this.loadCommentsFromServer, this.props.pollInterval );
    },

    render: function () {
        return (
            <div className="commentBox">
                <h1>
                    Comments
                </h1>
                <CommentList data={ this.state.data } />
                <CommentForm onCommentSubmit={ this.handleCommentSubmit } />
            </div>
        );
    }
});

React.render(
    <CommentBox url="comments.json" pollInterval={ 10000 } />,
    document.getElementById( 'content' )
);
