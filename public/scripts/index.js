// var converter = new showdown.Converter();
// 通过props，就能够从中读取到从 CommentList传递过来的数据 
// 任何内嵌的元素作为 this.props.children
var Comment = React.createClass({
    // var rawMarkup = converter.makeHtml( this.props.children.toString() );
    rawMarkup: function() {
        var rawMarkup = marked( this.props.children.toString(), { sanitize: true } );
        // console.log( rawMarkup );
        return { __html: rawMarkup };
    },

    render: function() {
        return (
            <div className="comment">
                <h2 className="commentAuthor">
                    { this.props.author }
                </h2>
                <span dangerouslySetInnerHTML={ this.rawMarkup() } />
            </div>
        );
    }
});
var data = [{
        author: "Pete Hunt",
        text: "This is oen comment"
    }, {
        author: "Jordan Walke",
        text: "This is *another* comment"
    }];

// 通过Javascript对象传递一些方法到React.createClassf() 来创建一个新的React组件。其中最重要的方法就是render，该方法返回一颗React组件树，这棵树最终将会渲染成HTML。
// 这个 <div> 标签不是真实的DOM节点；他们是React div组件的实例。React是安全的。我们不生成HTML字符串，因此默认阻止了XSS攻击。

var CommentList = React.createClass({
    render: function () {
        // console.log( this.props.data );
        var commentNodes = this.props.data.map( function ( comment ) {
            // console.log( comment.author );
            // console.log( comment.text );
            return (
                <Comment author={ comment.author }>
                    { comment.text }
                </Comment>
            );
        });
        return (
            <div className="commentList">
                { commentNodes }
            </div>
        );
    }    
});


var CommentForm = React.createClass({
    render: function () {
        return (
            <div className="commentForm">
                Hello, World! I am a CommentForm.
            </div>
        );
    }
});

var CommentBox = React.createClass({
    render: function () {
        return (
            <div className="commentBox">
                <h1>
                    Comments
                </h1>
                <CommentList data={ this.props.data } />
                <CommentForm />
            </div>
        );
    }
});

React.render(
    <CommentBox data={ data } />,
    document.getElementById( 'content' )
);
